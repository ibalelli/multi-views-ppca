import numpy as np
from csv import writer
import pandas as pd
from sklearn import preprocessing
from scipy.stats import matrix_normal, multivariate_normal
import os

def csv_for_records(script_dir):
    """
    This function creates csv for records
    :param script_dir: current directory
    :return csv files
    """
    
    # csv for saving stats for model performance
    results_csv_name = 'Model_comparison.csv'
    results_csv = os.path.join(script_dir, results_csv_name)
    if not os.path.isfile(results_csv):
        header = ['Test', 'Data', 'Norm', 'N_iter', 'D_k', 'N_subj', 'N_comp',
                  'MAE', 'MAE_test', 'accuracy_LDA_Test', 'conf_LDA_Test']

        with open(results_csv, "w", newline='') as f:
            writer(f, delimiter=',').writerow(header)  # write the header

    return results_csv

def append_list_as_row(file_name, list_of_elem):
    """
    This function is used to append results to csv
    :param file_name: str
    :param list_of_elem: list
    """
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)
