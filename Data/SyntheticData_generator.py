import numpy as np
import pandas as pd
import random
import os

def sample_x_n(N, q, random_state=None):
    return np.random.RandomState(random_state).randn(N,q)

def generate_data(N_g, W, a_g, mu, sigma2, x_n, random_state=None):
    rnd=np.random.RandomState(random_state)

    N=N_g.sum()
    d, q = W.shape
    sigma=np.sqrt(sigma2)

    return compute_mean_likelihood(N_g, W, a_g, mu, x_n) + sigma*rnd.randn(N,d)

def compute_mean_likelihood(N_g, W, a_g, mu, x_n):
    G=len(N_g)
    N=N_g.sum()
    d, q = W.shape

    g_ind=np.concatenate((np.zeros(1, dtype=np.int), np.cumsum(N_g)))


    y_n=np.empty((N, d))

    for g in range(G):
        y_n[g_ind[g]:g_ind[g+1]]= np.einsum("dq,nq->nd", W, x_n[g_ind[g]:g_ind[g+1]]+a_g[g]) + mu

    y_n = pd.DataFrame(data=y_n,
                     columns=[f'var_{i + 1}' for i in range(d)])

    return y_n

def MAR_data(y,perc):
    idx_list = y.index.tolist()
    remove_view_idx = random.sample(idx_list, int(perc*len(idx_list)))
    for i in remove_view_idx:
        y.iloc[i] = np.nan
    return y

np.random.seed(100)

D_i = [15, 8, 10]
G = 2

N_g = np.array([150,250])  # number of samples for each group

#
G = len(N_g)  # number of groups
g_ind = np.concatenate((np.zeros(1, dtype=np.int), np.cumsum(N_g)))
N = N_g.sum()

q_gen, sigma2_gen1, sigma2_gen2, sigma2_gen3 = 5, 2, 1, 3

W_gen1 = np.random.uniform(-10, 10, (D_i[0], q_gen))
W_gen2 = np.random.uniform(-5, 5, (D_i[1], q_gen))
W_gen3 = np.random.uniform(-15, 15, (D_i[2], q_gen))
a_g_gen = np.concatenate((np.zeros((1, q_gen)), np.random.uniform(-10, 10, (G - 1, q_gen))))
mu_gen1 = np.random.uniform(-10, 10, D_i[0])
mu_gen2 = np.random.uniform(-5, 5, D_i[1])
mu_gen3 = np.random.uniform(-15, 15, D_i[2])

x_n_gen = sample_x_n(N, q_gen, random_state=150)
y_t1 = generate_data(N_g, W_gen1, a_g_gen, mu_gen1, sigma2_gen1, x_n_gen, random_state=250)
y_t2 = generate_data(N_g, W_gen2, a_g_gen, mu_gen2, sigma2_gen2, x_n_gen, random_state=250)
y_t3 = generate_data(N_g, W_gen3, a_g_gen, mu_gen3, sigma2_gen3, x_n_gen, random_state=250)

y_t1 = MAR_data(y_t1,0.1)
y_t2 = MAR_data(y_t2,0.1)
y_t3 = MAR_data(y_t3,0.1)

gr = [0 for _ in range(N_g[0])]+[1 for _ in range(N_g[1])]
gr = pd.Series(gr)

T = pd.concat([gr, y_t1, y_t2, y_t3], axis=1)
print('HERE',T.columns.values)
T.columns.values[0] = 'Label'
T = T.dropna(axis=0, how='all')

print(T)
script_dir = os.path.dirname(__file__)
T.to_csv(script_dir + "/Synthetic_data_2g_missing10.csv")

True_params = [['W_1', W_gen1],['W_2', W_gen2],['W_3', W_gen3],['mu_1', mu_gen1],['mu_2', mu_gen2],['mu_3', mu_gen3]]
df = pd.DataFrame(True_params, columns =['Param', 'Value'])
df.to_csv(script_dir + "/true_params_SD_2g_missing10.csv")
