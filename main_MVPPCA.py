import sys
import pandas as pd
import seaborn as sns
import matplotlib
from math import sqrt, ceil
import numpy as np
import os
from os.path import dirname
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import confusion_matrix
from datetime import datetime
import argparse
from time import sleep
from random import randint
from copy import deepcopy
#from sys import getsizeof
sys.path.append(dirname(dirname(__file__)))

from utils import append_list_as_row, csv_for_records
from Model.mvPPCA import MVPPCA

matplotlib.use('agg')
sns.set()

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Fed_MVPPCA')
    parser.add_argument('--data', metavar='-d', type=str, default='SD', choices = ['SD', 'SD_missing', 'ADNI'],
                        help='Dataset')
    parser.add_argument('--n_iter', metavar='-MAP_i', type=int, default=15,
                        help='Number of iterations for EM (or MAP)')
    parser.add_argument('--latent_dim', metavar='-ld', type=int, default=2,
                        help='Dimension latent space')
    parser.add_argument('--id', metavar='-i', type=str, default='loc',
                        help='Test id')
    parser.add_argument('--norm', metavar='-n', type=int, default=1, choices = [0, 1],
                        help='normalize data. 0 = False; 1 = True')

    args = parser.parse_args()

    # csv file for records
    script_dir = os.path.dirname(__file__)

    # initialization
    n_comp = args.latent_dim  # number of components
    n_iterations = args.n_iter # number of EM iterations

    results_csv = csv_for_records(script_dir)

    print('==Loading and splitting data in train/test==')

    # Load data
    X = pd.read_csv('Data/Synthetic_data_2g.csv', index_col=0)
    # Remove spaces and lowercase feature names
    X.columns = [col.strip().lower() for col in list(X.columns)]
    X.rename(columns={'dx': 'label'}, inplace=True)

    train_data=X.sample(frac=0.8,random_state=200) #random state is a seed value
    test_data=X.drop(train_data.index)

    groups = np.unique(X['label'])  # total groups in dataset
    D_i = [15, 8, 10]
    G = len(groups)  # total number of groups
    K = len(D_i)  # total number of views

    # Id characterizing current test
    Test_id = datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '_' + args.id  # run id for records

    # Create datasets for saving results
    results = pd.DataFrame()  # results of local estimations

    # folder for saving figs
    name_res_dir = 'MVPPCA_Results_'+ Test_id + '/'
    results_dir = os.path.join(script_dir, name_res_dir)

    if not os.path.isdir(results_dir):
        os.makedirs(results_dir)

    XC = deepcopy(train_data)
    Y_train = XC.label
    XC = XC.drop(columns='label')
    Test_data = deepcopy(test_data)
    Y_test = Test_data.label
    Test_data = Test_data.drop(columns='label')

    N_subjects = XC.shape[0]
    N_iter = args.n_iter

    hyperpriors = None 

    # ====================================== #
    #          TRAINING STARTS HERE          #
    # ====================================== #
    print('===Start mv-PPCA training===')
    mvppca = MVPPCA(XC, norm=bool(args.norm),\
         dim_views=D_i, n_components=n_comp, n_iterations=n_iterations, )
    results = results.append(mvppca.fit(), ignore_index=True)
    muk, Wk, Sigma2 = mvppca.local_params
    # ====================================== #
    # ====================================== #

    # save local results to csv
    results.to_csv(results_dir + "mvppca_results.csv")

    print('==Generating and saving results==')

    Data = pd.DataFrame()
    Latent_Train = pd.DataFrame()
    Label_Train = pd.Series(dtype='int64')

    Latent_Train = Latent_Train.append(mvppca.simu_latent())
    Label_Train = Label_Train.append(Y_train)
    MAE=mvppca.MAE()

    lda = LinearDiscriminantAnalysis()
    Label_Train.index = Label_Train.index.map(str)
    Latent_Train.index = Latent_Train.index.map(str)
    Label_Train = Label_Train.loc[Latent_Train.index]
    Latent_Train = Latent_Train.sort_index()
    Label_Train = Label_Train.reindex(Latent_Train.index)#.sort_index()#
    X_Train_lda = lda.fit_transform(Latent_Train, Label_Train)

    # Evaluate global accuracy for test dataset
    Latent_Test = pd.DataFrame()
    Label_Test = pd.Series(dtype='int64')
    mvppca_test = MVPPCA(Test_data, norm=args.norm, dim_views=D_i,
                                    hyperpriors=[muk, Wk, Sigma2, None, None, None, None, None],
                                    n_components=n_comp, 
                                    n_iterations=0)
    # Dataframe of latent space for LDA
    Latent_Test = Latent_Test.append(mvppca_test.simu_latent(test=True))
    Label_Test = Label_Test.append(Y_test)
    # MAE Test
    MAE_test=mvppca_test.MAE(test=True)
    # LDA Test
    Size_tes = Latent_Test.shape[0]
    Label_Test.index = Label_Test.index.map(str)
    Latent_Test.index = Latent_Test.index.map(str)
    Label_Test = Label_Test.loc[Latent_Test.index]
    Latent_Test = Latent_Test.sort_index()
    Label_Test = Label_Test.reindex(Latent_Test.index)#.sort_index()#
    y_pred_test = lda.predict(Latent_Test)
    conf_LDA_Test = confusion_matrix(Label_Test, y_pred_test)
    TP = np.diag(conf_LDA_Test)
    num_classes = len(np.unique(Label_Test))
    accuracy_LDA_Test = sum(TP) / Size_tes

    # Append results to csv for model comparaison
    # List of strings
    row_contents = [Test_id, args.data, bool(args.norm), n_iterations, 
                    D_i, N_subjects, n_comp, MAE, MAE_test,
                    accuracy_LDA_Test, conf_LDA_Test]
    # Append row_contents as new line to Model_comparaison.csv
    sleep(randint(1,60))
    append_list_as_row(results_csv, row_contents)
