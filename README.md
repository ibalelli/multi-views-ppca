# Multi-View Probabilistic Principal Component Analysis (mv-PPCA)


mv-PPCA is an extension of the classical Probabilistic PCA of Tipping and Bishop allowing to taking into account multi-view data, under the assumption of a common latent space able to explain and generate every view-specific observation. Please refer to the original paper "A Probabilistic Framework for Modeling the Variability Across Federated Datasets of Heterogeneous Multi-View Observations" (Information Processing in Medical Imaging 2021) for all details on the method, which has been originally developped to work in a federated context (not considered here).

The current project only contains the elements needed for running multi-views PPCA on any multi-view dataset.

To run the main:

python3 main_MVPPCA.py --data 'SD' --latent_dim 5


