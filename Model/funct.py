import numpy as np
from numpy.linalg import solve, multi_dot
import pandas as pd
from sklearn import preprocessing
from typing import List

def normalize_data(X):
    """
    This function normalize the dataset X
    :param X: pandas dataframe
    :return pandas dataframe
    """
    col_name = [col.strip() for col in list(X.columns)]
    x = X.values  # returns a numpy array
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    norm_dataset = pd.DataFrame(x_scaled, index=X.index, columns=col_name)

    return norm_dataset

def view_data(n_views,dim_views,ViewsX,dataset,norm) -> List:
    """
    This function return a list containing as k-th element the local dataset for the k-th view
    :param n_views: integer
    :param dim_views: list (views dimensions)
    :param ViewsX: binary list (indicator functions for observed views)
    :param dataset: pandas dataframe
    :param norm: boolean indicating if data should be normalized
    :return list of pandas dataframes
    """
    X_obs = normalize_data(dataset) if norm else dataset
    Xk = []
    # self.Xk is a list contianing the view-specific local datasets
    ind = 0
    for k in range(n_views):
        if ViewsX[k] == 1:
            Xk.append(X_obs.iloc[:, ind:ind + dim_views[k]])
            ind += dim_views[k]
        else:
            Xk.append(np.nan)
    return X_obs, Xk

def compute_Cck(Wk, Sigk):
    """
    Computes matrix Ck.
    :param Wk: matrix (d_k x q)
    :param Sigk: float > 0
    :return np.array : matrix Cck
    """

    dk = Wk.shape[0]

    Cck = np.dot(Wk,Wk.T)+Sigk*np.eye(dk)

    return Cck

def compute_inv_term1_muck(Wk, Nc, Sigk, Til_Sigk):
    """
    :param Wk: matrix (d_k x q)
    :param Nc: float > 0
    :param Sigk: float > 0
    :param Til_Sigk: float > 0
    :return np.array : inverse of first term to evaluate muck, using the Woodbury matrix identity
    """

    dk = Wk.shape[0]
    q = Wk.shape[1]

    k = 1.0/(Nc*Til_Sigk+Sigk)

    Inverse = solve(np.eye(q)+k*np.dot(Wk.T,Wk), np.eye(q))

    inverse_term1 = k*Til_Sigk*(np.eye(dk)-k*multi_dot([Wk,Inverse,Wk.T]))

    return inverse_term1

