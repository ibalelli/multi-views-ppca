import numpy as np
from numpy.linalg import solve, LinAlgError
import pandas as pd
from math import log, sqrt, exp 
from sklearn.metrics import mean_absolute_error
from scipy.linalg import block_diag
from scipy.special import logsumexp
from scipy.stats import matrix_normal, multivariate_normal, invgamma, laplace
from copy import deepcopy

from .funct import view_data, compute_Cck, compute_inv_term1_muck

class MVPPCA:
    """
    This class represents the local optimization in each training center
    """
    def __init__(self,  
                 data,
                 norm: bool=True, 
                 ViewsX: list=None, 
                 dim_views: list=None, 
                 n_components: int=None, 
                 hyperpriors: list=None, 
                 n_iterations: int=None, 
                 ):

        """ Constructor of the class

        Args:
            data (pandas df): pandas multi-view dataframe
            norm (bool): boolean to decide if the dataset should be normalized or not. Defaults to True.
            ViewsX (list): list containing 1 at position i if the center id_client disposes of data for the i-th view,
                    0 otherwise. len(ViewsX)=tot num views. Defaults to None
            dim_views (list): list containing the dimension of each view, len(dim_views)=tot num views.
                              Defaults to None
            n_components (int): the latent dimension. Defaults to None
            hyperpriors (list): list containing the optimized gobal parameters at the last iteration.
                                  Defaults to None
            n_iterations (int): the number of EM/MAP iterations for the current round. Defaults to None
        """

        self.dim_views = dim_views
        self.n_views = len(self.dim_views)
        self.ViewsX = [1 for _ in range(self.n_views)] if ViewsX is None else ViewsX
        self.index = self.ViewsX.index(1)  # first view measured (jsut for testing)
        self.n_components = n_components
        self.n_iterations = n_iterations

        # verifications:
        assert self.n_views == len(self.ViewsX), 'List of indicator functions for observed views must have the same lengh '\
                                                 'as the number of views'

        D_i_C = 0
        for k in range(self.n_views):
            if self.ViewsX[k] == 1:
                D_i_C += self.dim_views[k]

        assert data.shape[1] == D_i_C, 'Dataset size must be equal to sum of views dimensions'

        # self.Xk is a list contianing the view-specific local datasets
        self.dataset, self.Xk = view_data(self.n_views,self.dim_views,self.ViewsX,data,norm)
        #for k in range(self.n_views):
        #    if self.ViewsX[k] == 1:
                #print(self.Xk[k].head())
                #print(f'Shape of X%i: {self.Xk[k].shape}' % k)

        # The "effective" latent space dimension per view (at least equal to original view dimension-1):
        # if q>dim_views[k], than all columns of W_k with index >=q will be set to 0
        self.q_i = self.effectitve_latent_dim()

        if self.n_components > min(self.dim_views):
            print("Warning: number of components greater than view dimensions")
            print(self.q_i)

        # hyperpriors to be used for MAP estimation, if provided
        self.hyperpriors = hyperpriors 
        if self.hyperpriors is not None:
            self.tilde_muk = self.hyperpriors[0]
            self.tilde_Wk = self.hyperpriors[1]
            self.tilde_Sigma2k = self.hyperpriors[2]
            self.Alpha = self.hyperpriors[3]
            self.Beta = self.hyperpriors[4]
            self.sigma_til_muk = self.hyperpriors[5]
            self.sigma_til_Wk = self.hyperpriors[6]
            self.sigma_til_sigma2k = self.hyperpriors[7]
            for k in range(self.n_views):
                if self.ViewsX[k] == 1:
                    self.tilde_muk[k] = deepcopy(self.tilde_muk[k].reshape(self.dim_views[k], 1))
                    self.tilde_Wk[k] = deepcopy(self.tilde_Wk[k].reshape(self.dim_views[k], self.n_components))

        # model parameters to be optimized
        self.muk = None
        self.Wk = None
        self.Sigma2 = None

        # Expected log likelihood
        self.E_L_c = None

    def fit(self):
        """
        This function perform the local EM/MAP optimization. 
        It returns a dataframe with information of convergence for each parameter, 
        and the likelihood.
        :return pandas dataframe
        """

        N = self.dataset.shape[0]

        # initialize W and Sigma2
        Wk, Sigma2 = self.initial_loc_params()

        # =================================== #
        # Create dataset for historic results #
        # =================================== #
        # epochs indices for saving results
        sp_arr = np.arange(1, self.n_iterations + 1)[np.round(
            np.linspace(0, len(np.arange(1, self.n_iterations + 1)) - 1, int(self.n_iterations / 5))).astype(
            int)]
        # Create list of historic results as dataframe
        results_df = pd.DataFrame()

        # ================================== #
        #  ITERATION STARTS HERE             #
        # ================================== #
        for i in range(1, self.n_iterations + 1):
            muk, Wk, Sigma2, ELL = self.EM_Optimization(N,Wk,Sigma2,self.Xk)

            # ================================== #
            # Append results                     #
            # ================================== #
            if i in sp_arr:
                print(f'====> Epoch {i}/{self.n_iterations}, ELL = {ELL}')
                results_iter = self.append_results_local(i, ELL, Wk, muk, Sigma2)
                results_df = results_df.append(results_iter)

        # update local parameters
        self.muk = muk
        self.Wk = Wk
        self.Sigma2 = Sigma2
        self.E_L_c = ELL

        return results_df

    @property
    def local_params(self):
        """
        :return three lists: optimized parameters
        """
        return self.muk, self.Wk, self.Sigma2

    @property
    def LL(self):
        """
        :return float: expected total loglikelihood 
        """
        return self.E_L_c

    def effectitve_latent_dim(self):
        """
        This function returns a list with the effective latent dimension to be considered for each view:
        if the dimension of view k d_k >= q, the user defined latent dimension, hence only the first d_k-1 
        column of W_k will be optimized, the remaining one will be set to 0.
        :return list
        """
        q_i = [] 
        for i in self.dim_views:
            if i <= self.n_components:
                q_i.append(i - 1)
            else:
                q_i.append(self.n_components)
        return q_i

    def WAIC_score(self, S):
        """
        This function returns the WAIC score
        :param S: integer (number of simulations for WAIC evaluation)
        :return float WAIC
        :return integer min(len_logP_N)
        """
        d = self.dataset.shape[1]
        N = self.dataset.shape[0]

        elppd2 = 0.0

        logP_N = [[] for _ in range(N)]
        for s in range(S):
            Cs, mus = self.Sample_C_musk()
            for n in range(N):
                try:
                    logP_N[n].append(
                        multivariate_normal.logpdf(self.dataset.iloc[n].values.reshape(d), mus.reshape(d), Cs))
                except LinAlgError:
                    pass

        len_logP_N = [len(logP_N[n]) for n in range(N)]

        for n in range(N):
            if len(logP_N[n])>0:
                LSE = logsumexp(logP_N[n])
                VarP = np.var(logP_N[n])
                elppd2 += LSE - VarP - log(len_logP_N[n])

        if min(len_logP_N) < S:
            print(f'Number of simulations for WAIC: {min(len_logP_N)}')

        WAIC = -2*elppd2

        return WAIC, min(len_logP_N)

    def Sample_C_musk(self):
        """
        This function returns the dxd covariance matrix C (WWT+Psi)Wk, 
        and dx1 vector mu (needed for WAIC score evaluation)
        :return np.array (d x d)
        :return np.array (d x 1)
        """
        K = self.n_views
        D_i = self.dim_views

        Wsk, musk, sigmaks = self.Sample_LP(self.index)
        Psi = sigmaks * np.eye(D_i[self.index])
        for k in range(self.index + 1, K):
            if self.ViewsX[k] == 1:
                Wsk_k, musk_k, sigmaks = self.Sample_LP(k)
                Wsk = np.concatenate((Wsk, Wsk_k), axis=0)
                musk = np.concatenate((musk, musk_k), axis=0)
                Psi = block_diag(Psi, sigmaks * np.eye(D_i[k]))
        C = Wsk.dot(Wsk.T) + Psi

        return C, musk

    def Sample_LP(self,k):
        """
        This function, for a given view k, returns Wk, muk and sigmak sampled from their distribution
        :param k: integer corresponding to the view to sample
        :return np.array Wk (d_k x q)
        :return np.array muk (d_k x 1)
        :return float sigmak > 0
        """
        q = self.n_components
        D_i = self.dim_views

        Wk = matrix_normal.rvs(mean=self.tilde_Wk[k],
                                rowcov=np.eye(D_i[k]),
                                colcov=self.sigma_til_Wk[k] * np.eye(q)).reshape(D_i[k], q)
        muk = multivariate_normal.rvs(mean=self.tilde_muk[k].reshape(D_i[k]),
                                       cov=self.sigma_til_muk[k] * np.eye(D_i[k])).reshape(
            D_i[k], 1)
        sigmak = float(invgamma.rvs(a=self.Alpha[k], scale=self.Beta[k]))

        return Wk, muk, sigmak

    def simuXn(self):
        """
        This function generates x_n using p(x_n|t_n) and optimized parameters.
        :return np.array 
        """
        d = self.dataset.shape[1]
        N = self.dataset.shape[0]

        mu = self.concat_params(self.muk)
        M, B = self.eval_MB(self.Wk, self.Sigma2)

        ran_n = np.random.randint(0, N - 1)

        Xng = np.random.multivariate_normal(mean=list((M.dot(B).dot(self.dataset.iloc[ran_n].values.reshape(d, 1) - mu)).T[0]), cov=M)

        return Xng

    def MAE(self, test=False):
        """
        This function evaluates the MAE using optimized parameters
        :return float
        """

        T_true = self.dataset.values.tolist()
        T_pred = self.simu_tn(test).values.tolist()

        MAE = mean_absolute_error(T_true, T_pred)

        return MAE

    def simu_tn(self, test=False):
        """
        This function evaluates the MAE using optimized parameters
        :return pandas dataframe
        :param test: boolean. If test=True, the hyperpriors are used for simulation
        """
        d = self.dataset.shape[1]
        q = self.n_components
        N = self.dataset.shape[0]

        if test:
            mu = self.concat_params(self.tilde_muk)
            W = self.concat_params(self.tilde_Wk)
            M, B = self.eval_MB(self.tilde_Wk, self.tilde_Sigma2k)
        else:
            mu = self.concat_params(self.muk)
            W = self.concat_params(self.Wk)
            M, B = self.eval_MB(self.Wk, self.Sigma2)

        T_pred = []
        for n in range(N):
            Xng = (M.dot(B).dot(self.dataset.iloc[n].values.reshape(d, 1) - mu)).reshape(q, 1)
            T_pred.append((W.dot(Xng) + mu).reshape(d))
        
        T_pred_df = pd.DataFrame(np.vstack(T_pred))

        return T_pred_df

    def simu_latent(self, test=False):
        """
        This function allows sampling of x_n (latent variables) from the posterior distribution.
        :param test: boolean. If test=True, the hyperpriors are used for simulation
        :return pandas dataframe
        """
        d = self.dataset.shape[1]
        q = self.n_components
        N = self.dataset.shape[0]

        if test:
            mu = self.concat_params(self.tilde_muk)
            M, B = self.eval_MB(self.tilde_Wk, self.tilde_Sigma2k)
        else:
            mu = self.concat_params(self.muk)
            M, B = self.eval_MB(self.Wk, self.Sigma2)

        Xn = [(M.dot(B).dot(self.dataset.iloc[n].values.reshape(d, 1) - mu)).reshape(1, q) for n in range(N)]

        df = pd.DataFrame(np.vstack(Xn), index=self.dataset.index)

        return df

    def concat_params(self, park):
        """
        This function concatenates parameters from a list
        :param park: list of vectors/matrices to concatenate
        :return np.array
        """
        K = self.n_views

        par = np.concatenate([park[k] for k in range(K) if self.ViewsX[k] == 1], axis=0)

        return par

    def pred_missing_view(self,view):
        """
        This function predicts missing views for each subject.
        :param view: integer corresponding to the view to be predicted
        :return pandas dataframe
        """
        d = self.dataset.shape[1]
        D_i = self.dim_views
        q = self.n_components
        N = self.dataset.shape[0]

        mu = self.concat_params(self.muk)
        M, B = self.eval_MB(self.Wk, self.Sigma2)

        T_pred = []
        for n in range(N):
            Xng = (M.dot(B).dot(self.dataset.iloc[n].values.reshape(d, 1) - mu)).reshape(q, 1)
            T_pred.append((self.Wk[view].dot(Xng) + self.muk[view]).reshape(D_i[view]))

        df = pd.DataFrame(np.row_stack(T_pred))

        return df

    def EM_Optimization(self,N,Wk,Sigma2,Xk):
        """
        This function manage one iteration of EM/MAP optimization
        :param N: integer (number of subjects in the dataset)
        :param Wk: list of matrices (d_k x q)
        :param Sigma2: list of floats > 0
        :param Xk: list of view-specific datasets
        :return list of np.arrays (d_k x 1) muk
        :return list of np.arraya (d_k x q) Wk
        :return list of floats > 0 Sigma2
        :return floaT E_L_c
        """
        K = self.n_views

        # mu
        muk = self.eval_muk(N, Wk, Sigma2, Xk)
        # matreces M, B
        M, B = self.eval_MB(Wk, Sigma2)
        # ||(tn^kg-mu^k)||2, (tn^kg-mu^k)
        norm2, tn_muk = self.compute_access_vectors(N,muk,Xk)

        # ================================== #
        # E-step                             #
        # ================================== #

        # evaluate mean, second moment for each x_n and expected complete log-likelihood
        E_X, E_X_2, E_L_c = self.compute_moments_LL(N, Sigma2, norm2, tn_muk, Wk, M, B)

        # ================================== #
        # M-step                             #
        # ================================== #

        #  W, Sigma2
        Wk, Sigma2_new = self.eval_Wk_Sigma2_new(N, norm2, tn_muk, E_X, E_X_2, Sigma2, M)
        # Check Sigma2_new>0
        for k in range(K):
            if self.ViewsX[k] == 1:
                if Sigma2_new[k] > 0:
                    Sigma2[k] = Sigma2_new[k]
                else:
                    print(f'Warning: sigma2(%i)<0 (={Sigma2_new[k]})' % (k + 1))

        return muk, Wk, Sigma2, E_L_c

    def initial_loc_params(self):
        """
        This function initializes Wk and Sigmak.
        :return list of np.arrays (d_k x q) Wk
        :return list of floats > 0 Sigma2
        """
        q = self.n_components
        D_i = self.dim_views
        K = self.n_views

        Wk = []
        Sigma2 = []
        for k in range(K):
            W_k, s = self.PCA_init_Wk_sigmak(k)
            if ((self.hyperpriors is not None) and (self.tilde_Wk[k] is not None)):
                W_k = matrix_normal.rvs(mean=self.tilde_Wk[k].reshape(D_i[k], q),
                                                rowcov=np.eye(D_i[k]),
                                                colcov=self.sigma_til_Wk[k]*np.eye(q)).reshape(D_i[k], q)
            
            if self.q_i[k] < q:
                W_k[:, self.q_i[k]:q] = 0
            if ((self.hyperpriors is not None) and (self.tilde_Sigma2k[k] is not None)):
                s = float(invgamma.rvs(a=self.Alpha[k], scale=self.Beta[k]))
            Wk.append(W_k)
            Sigma2.append(s)

        return Wk, Sigma2

    def PCA_init_Wk_sigmak(self,k):
        from sklearn.decomposition import PCA
        Xk = deepcopy(self.Xk[k])
        Xk_norm=((Xk - Xk.mean()) / Xk.std()).to_numpy()
        Sigma2 = Xk_norm.std()
        pca = PCA(n_components=self.n_components)
        pca.fit(Xk_norm)
        eigenvector = pca.components_.T
        eigenvalues = pca.explained_variance_
        sigma_vec = Sigma2*np.ones(self.n_components)
        eigen_val_Wk = np.subtract(eigenvalues, sigma_vec)#.clip(min=0)
        Wk = np.dot(eigenvector.reshape(self.dim_views[k], self.n_components),np.diag(eigen_val_Wk))\
            .reshape(self.dim_views[k], self.n_components)

        return Wk, Sigma2

    def eval_muk(self,N,Wk,Sigma2,Xk):
        """
        Computes parameter muk.
        :param N: integer (number of subjects)
        :param Wk: list of matrices (d_k x q)
        :param Sigma2: list of float > 0
        :param Xk: list of view-specific datasets
        :return list of np.arrays
        """

        D_i = self.dim_views
        K = self.n_views

        muk = []

        for k in range(K):
            if ((self.hyperpriors is None) or (self.tilde_muk[k] is None)):
                mean_k = Xk[k].mean(axis=0).values.reshape(self.dim_views[k], 1)
                muk.append(mean_k)
            else:
                mu_1 = np.zeros((D_i[k], 1))
                for n in range(N):
                    mu_1 += Xk[k].iloc[n].values.reshape(D_i[k], 1)
                term1 = compute_inv_term1_muck(Wk[k], N, Sigma2[k], self.sigma_til_muk[k])
                Cc = compute_Cck(Wk[k], Sigma2[k])
                term2 = mu_1 + (1 / self.sigma_til_muk[k]) * Cc.dot(self.tilde_muk[k].reshape(D_i[k], 1))
                muk.append(term1.dot(term2))

        return muk

    def eval_MB(self, Wk, Sigma2):
        """
        Computes matrices M:=inv(I_q+sum_k Wk.TWk/sigma2k) and B:= [W1.T/sigma2K,...,W1.T/sigma2K].
        :param Wk: list of matrices (d_k x q)
        :param Sigma2: list of float > 0
        :return np.arrays
        """

        q = self.n_components
        D_i = self.dim_views
        K = self.n_views

        M1 = Wk[self.index].reshape(D_i[self.index], q).T.dot(Wk[self.index].reshape(D_i[self.index],q)) / Sigma2[self.index]
        B = Wk[self.index].reshape(D_i[self.index], q).T / Sigma2[self.index]
        for k in range(self.index + 1, K):
            if self.ViewsX[k] == 1:
                M1 += Wk[k].reshape(D_i[k], q).T.dot(Wk[k].reshape(D_i[k],q)) / Sigma2[k]
                B = np.concatenate((B, (Wk[k].reshape(D_i[k], q)).T / Sigma2[k]), axis=1)

        M = solve(np.eye(q) + M1,np.eye(q))

        return M, B

    def compute_access_vectors(self,N,muk,Xk):
        """
        Computes list of accessory vectors needed for further evaluations: 
        norm**2 of (tn^kg-mu^k-W^kag), (tn^kg-mu^k-W^kag), (tn^kg-mu^k).
        :param N: integer (number of subjects)
        :param muk: list of vectors (d_k x 1)
        :param Xk: list of view-specific datasets
        :return list of floats norm2
        :return list of np.arrays
        """

        D_i = self.dim_views
        K = self.n_views

        norm2 = [] # norm**2 of (tn^kg-mu^k)
        tn_muk = [] # (tn^kg-mu^k)
        for n in range(N):
            norm2_k = []
            tn_mu_k = []
            for k in range(K):
                if self.ViewsX[k] == 1:
                    tn_mu_k.append(Xk[k].iloc[n].values.reshape(D_i[k], 1) - muk[k])
                    norm2_k.append(np.linalg.norm(tn_mu_k[k]) ** 2)
                else:
                    norm2_k.append(np.nan)
                    tn_mu_k.append(np.nan)
            norm2.append(norm2_k)
            tn_muk.append(tn_mu_k)

        return norm2, tn_muk

    def compute_moments_LL(self, N, Sigma2, norm2, tn_muk, Wk, M, B):
        """
        Computes mean and second moment for each x_n, then expected log-likelihood.
        :param N: integer (number of subjects)
        :param Sigma2: list of floats > 0
        :param norm2: list of floats: norm**2 of (tn^kg-mu^k)
        :param tn_muk: list of vectors (d_k x 1)
        :param Wk: list of matrices (d_k x q)
        :param M: matrix (q x q)
        :param B: matrix (q x d_k)
        :return list of np.arrays E_X
        :return list of np.arrays E_X_2
        :return float E_L_c
        """

        q = self.n_components
        D_i = self.dim_views
        K = self.n_views

        E_X = []
        E_X_2 = []
        E_L_c = 0.0

        for n in range(N):
            tn_mu = tn_muk[n][self.index]
            for k in range(self.index+1,K):
                if self.ViewsX[k] == 1:
                    tn_mu = np.concatenate((tn_mu, tn_muk[n][k]), axis=0)
            E_X.append(M.dot(B).dot(tn_mu))

            E_X_2.append(M + E_X[n].dot(E_X[n].T))

            E_L_c_k = 0.0
            for k in range(K):
                E_L_c_k += - D_i[k] * log(Sigma2[k])/ 2.0 - (norm2[n][k] / 2 + np.matrix.trace(
                    (Wk[k].reshape(D_i[k], q)).T.dot(Wk[k].reshape(D_i[k], q)) * E_X_2[n]) / 2 - E_X[n].T.dot(
                    (Wk[k].reshape(D_i[k], q)).T).dot(tn_muk[n][k])) / Sigma2[k]
            E_L_c += float(E_L_c_k  - np.matrix.trace(E_X_2[n]) / 2.0)

        return E_X, E_X_2, E_L_c

    def eval_Wk_Sigma2_new(self, N, norm2, tn_muk, E_X, E_X_2, Sigma2, M):
        """
        Computes local parameter Wk and Sigma2k.
        :param N: integer (number of subjects)
        :param norm2: list of floats: norm**2 of (tn^kg-mu^k)
        :param tn_muk: list of vectors (d_k x 1): (tn^kg-mu^k)
        :param E_X: list of vectors (q x 1): mean of x_n
        :param E_X_2: list of matrices (q x q): second moment of x_n
        :param Sigma2: list of floats > 0
        :return list of np.arrays Wk
        :return list of floats > 0
        """

        q = self.n_components
        D_i = self.dim_views
        K = self.n_views

        Wk = []
        Sigma2_new = []

        for k in range(K):
            W_1_1 = (tn_muk[0][k]).dot(E_X[0].T)
            W_2_2 = sum(E_X_2)
            for n in range(1,N):
                W_1_1 += (tn_muk[n][k]).dot(E_X[n].T)

            if ((self.hyperpriors is None) or (self.tilde_Wk[k] is None)):
                W_1 = W_1_1

                W_2 = solve(W_2_2, np.eye(q))
            else:
                W_1 = W_1_1 + (Sigma2[k] / self.sigma_til_Wk[k]) * self.tilde_Wk[k]

                W_2 = solve(W_2_2 + (Sigma2[k] / self.sigma_til_Wk[k]) * np.eye(q), np.eye(q))

            W_k = W_1.dot(W_2)
            if self.q_i[k] < q:
                W_k[:, self.q_i[k]:q] = 0
            Wk.append(W_k)

            sigma2k = 0.0
            for n in range(N):
                sigma2k += float(np.linalg.norm(tn_muk[n][k]-(Wk[k].reshape(D_i[k], q)).dot(E_X[n]))**2+\
                    np.matrix.trace((Wk[k].reshape(D_i[k], q)).dot(M.dot((Wk[k].reshape(D_i[k], q)).T))))
            if ((self.hyperpriors is None) or (self.tilde_Sigma2k[k] is None)):
                sigma2k_opt = deepcopy(sigma2k)
                sigma2k_opt /= (N * D_i[k])
                if sigma2k_opt>0:
                    Sigma2_new.append(sigma2k_opt)
                else:
                    var = 1  # variance of the Inverse-Gamma prior
                    alpha = 1.0 / (4 * var) + 2
                    beta = (alpha - 1) / 2
                    sigma2k_N = (sigma2k + 2 * beta) / (N * D_i[k] + 2 * (alpha + 1))  ## prior=inverse gamma
                    while sigma2k_N <= 0:  # while til obtention of a non negative sigma2k: each round the variance of the Inverse Gamma is divided by 2
                        var *= 1.0 / 2
                        alpha = 1.0 / (4 * var) + 2
                        beta = (alpha - 1) / 2
                        sigma2k_N = (sigma2k + 2 * beta) / (N * D_i[k] + 2 * (alpha + 1))  ## prior=inverse gamma
                    if var != 1:
                        print(f'Variance of Inverse-Gamma for sigma2(%i) = {var}' % (k + 1))
                    Sigma2_new.append(sigma2k_N)
            else:
                Sigma2_new.append((sigma2k + 2 * self.Beta[k]) / (N * D_i[k] + 2 * (self.Alpha[k] + 1)))

        return Wk, Sigma2_new

    def append_results_local(self, ep, E_L_c, Wk, muk, Sigma2):
        """
        Append all results for record.
        :param ep: int (current epoch)
        :param E_L_c: float (expected log-likelihood)
        :param Wk: list of matrices (d_k x q)
        :param muk: list of vectors (d_k x 1)
        :param Sigma2: list of floats > 0
        :return pandas series
        """

        K = self.n_views

        results_iter = pd.Series(name=ep, dtype=float)
        results_iter['iter'] = ep
        results_iter['E_L_c'] = E_L_c
        for k in range(K):
            name1 = 'W_norm_' + str(k + 1)
            name2 = 'mu_norm_' + str(k + 1)
            name3 = 'sigma2_' + str(k + 1)
            results_iter[name1] = np.linalg.norm(Wk[k])
            results_iter[name2] = np.linalg.norm(muk[k])
            results_iter[name3] = Sigma2[k]

        return results_iter
